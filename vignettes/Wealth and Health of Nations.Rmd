---
title: "Wealth and Health of Nations"
author: "Luke Heley"
date: "`r Sys.Date()`"
output: rmarkdown::html_vignette
vignette: >
  %\VignetteIndexEntry{Wealth and Health of Nations}
  %\VignetteEngine{knitr::rmarkdown}
  %\VignetteEncoding{UTF-8}
---

```{r, warning=F, message=F}
require(r2d3MotionChart)
r2d3MotionChart()

```

This is a port of Mike Bostock's [D3 recreation](https://bost.ocks.org/mike/nations/) of Gapminder's Wealth and Health of Nations. It shows the dynamic fluctuation in per-capita income (x), life expectancy (y) and population (radius) of 180 nations over the last 209 years. Nations are colored by geographic region; mouseover to read their names. 
